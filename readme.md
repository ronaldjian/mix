# Laravel Mix Without Laravel

[Laravel Mix docs](https://github.com/JeffreyWay/laravel-mix/tree/master/docs#readme)

## Installation

To install all dependencies:

```
npm install
```

## Usage

Run all tasks:

```
npm run dev
```

Run all tasks and minify:

```
npm run production
```

Run all tasks and watch for changes:

```
npm run watch
```

## Vue 參考資料
[awesome-vue](https://github.com/vuejs/awesome-vue)

[前後端分離與 SPA](https://blog.techbridge.cc/2017/09/16/frontend-backend-mvc/)

[不可不知的 JavaScript 趨勢 - Isomorphic 與 UniversalJS](http://v123582.github.io/blog/2016/04/03/%E4%B8%8D%E5%8F%AF%E4%B8%8D%E7%9F%A5%E7%9A%84%E5%89%8D%E7%AB%AF%E8%B6%A8%E5%8B%A2-Isomorphic-%E8%88%87-UniversalJS/)


### 設定虛擬機器以及hosts設定
[https://jason.pureconcepts.net/2014/11/configure-apache-virtualhost-mac-os-x/](https://jason.pureconcepts.net/2014/11/configure-apache-virtualhost-mac-os-x/)



